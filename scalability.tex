\begin{figure*}
  \centering
\captionsetup{justification=centering}
\includegraphics[width=.95\textwidth]{figures/scalability-tcp-flows/cwnd-400Mbps-UE6_1.eps}
\caption{TCP Congestion Window evolution with 1200 Mb/s of UDP and 24 additional TCP flows.}
\label{fig:tcp-cwindow}
\end{figure*}

\begin{figure*}
\centering
\captionsetup{justification=centering}
\includegraphics[width=.95\textwidth]{figures/scalability/finishtime.eps}
\caption{File download time (TCP) and UDP average backhaul throughput,
  presented while increasing UDP backhaul traffic.}
\label{fig:finish_scala}
\end{figure*}

\begin{figure*}
\centering
\captionsetup{justification=centering}
\includegraphics[width=.95\textwidth]{figures/scalability/rtt.eps}
\caption{TCP RTT and delivery ratio while increasing UDP backhaul traffic.}
\label{fig:srtt_scala}
\end{figure*}

\begin{figure*}
\centering
\captionsetup{justification=centering}
\includegraphics[width=.95\textwidth]{figures/scalability-tcp-flows/download-200Mbps.eps}
\caption{File download time and RTT of up to 100 TCP file transfers with 600 Mb/s of UDP traffic.}
\label{fig:tcp-scala-600}
\end{figure*}

%\begin{figure*}
%\centering
%\captionsetup{justification=centering}
%\includegraphics[width=.95\textwidth]{figures/scalability-tcp-flows/download-400Mbps.eps}
%\caption{File download time and RTT of up to 100 TCP file transfers with 1200 Mb/s of UDP traffic.}
%\label{fig:tcp-scala-1200}
%\end{figure*}


In this section, we evaluate the routing strategies by analyzing the TCP
performance of 25 LTE devices concurrently downloading a 10 MB file from the
Internet. We present different set of results, gathered by keeping fixed the
number of LTE devices but increasing UDP workload in the backhaul network. In
Figure~\ref{fig:finish_scala} and in Figure~\ref{fig:srtt_scala} we can see the
download time values (i.e. the time that the TCP implementation takes to deliver
completely and in-order all bytes to the application) and their average RTT,
respectively. The values are represented in candlesticks, where the box
stretches from the 20th to the 80th percentiles and the whiskers represent the
maximum and minimum values. In Figure~\ref{fig:finish_scala}, we also plot the
percentage of achieved UDP traffic. For instance, a value of 80\% means that, in
average, only 80\% of nominal UDP traffic has reached the destination. For sake
of completeness, we added in Figure~\ref{fig:srtt_scala} the percentage of bytes
correctly delivered to the applications (TCP delivery ratio). Since all values
are fixed to 100\%, it means that (for every flow) the TCP protocol has been
able to entirely deliver, using also retransmissions, the 10 MB file.

\textbf{OLSR}. It represents the worst-case in terms of per-SC resource usage. In fact, the
protocol specifications lead to routing the data always on the shortest path,
irrespective of the congestion level. We observe in
Figure~\ref{fig:finish_scala} that when the UDP backhaul traffic is not
sustainable anymore (i.e., from 960 Mb/s onward), OLSR experiences the lowest
throughput values. A look to reference TCP traffic shows that OLSR experiences
the lowest degree of RTT variability compared to \emph{per-packet} and
\emph{per-flow} but higher download times than \emph{per-flow}. The reason for
this is the congestion unawareness of OLSR: as soon as the per-interface queue
reaches its limit, packets begin to be dropped causing the TCP congestion
control algorithm to limit TCP rate. Therefore, under high loads, statically
choosing paths independently of the congestion level leads to inefficient
resource usage (lower UDP throughput, increased packet drops) and constant
delays, which are a function of the maximum queue size and the number of hops
traversed. This is why the average file download time (black horizontal line in
the candlesticks) does not change with the backhaul traffic load in
Figure~\ref{fig:finish_scala}.

\textbf{BP-MR per-packet}. Taking backpressure routing decisions on a
\emph{per-packet} basis attains the best results for aggregated UDP throughput
but high inefficiencies to complete all the TCP file transfers, regardless of
the UDP workload. For instance, for an UDP workload of 1080 Mb/s
\emph{per-packet} variant attains a 90\% of injected UDP traffic, while TCP
transfers average finish time is 30 seconds. The finish time trend has a nearly
constant average through all the UDP workload configurations but it presents a
high degree of variability, represented by the bigger candlesticks, indicating a
poor fairness between different TCP flows. There is more variability, with
respect to OLSR, also in the RTT distribution. Average values are however close
to the ones achieved with OLSR, but we need to consider that, giving the high
dynamism of the \emph{per-packet} routing strategy, segments of the same TCP
flow can in fact experience a different number of hops, leading to a reordering
problem at the receiver, that is the main limiting factor for the original
\emph{BP-MR} strategy and many other backpressure strategies. To recap,
\emph{per-packet} routing strategy is able to utilize resources more efficiently
in presence of congestion (higher backhaul UDP throughput) at the cost of
penalizing TCP flows, due to serious reordering issues.

\textbf{BP-MR per-flow}. Taking decisions on a \emph{per-flow} basis attains the
best trade-off among UDP and TCP performance, showing
significant TCP improvements compared to OLSR and \emph{per-packet} flavors,
experiencing also close-to the best results in terms of UDP backhaul traffic.
This is because \emph{per-flow} computes a new load balancing decision for every
new flow injected in the network and sticks with this decision. The former
feature allows calculating the less loaded fixed path, hence making the most out
of the wireless backhaul resources, whereas the latter feature minimizes packet
reordering. With less reordering and less packet drops, the finish time is in
general really low and from Figure~\ref{fig:finish_scala} it is clearly visible
that it outperforms both OLSR and \emph{per-packet}, even considering flow
fairness (the candlesticks are condensed in a very tight space). For what
regards RTT distribution, it is  higher than the other two flavors, but we need
to take into account the fact that lost, retransmitted, and out-of-order segments
do not contribute to the RTT calculation in the senders, and thus they are not
represented in that graph. With \emph{per-flow} strategy, we can infer from the
finish time performance a really low number of lost and out-of-order segments,
and so a higher number of RTT samples. At the end, \emph{per-flow} strategy is
able to circumvent congestion (and deliver more backhaul traffic than OLSR)
without penalizing TCP performance, as it can be expected from a strategy that
blends the best from OLSR (fixed paths) and \emph{per-packet} (dynamic
congestion avoidance) routing strategy.

To achieve the same performance, in these loaded conditions with a static
protocol such as OLSR, a network architect should have increased the allocated
network resources. Using \emph{per-flow} routing strategy however, the architect
is able to exploit resources even when the backhaul traffic is approaching the
network saturation; in the following, we will analyze the protocol response with
an increased number of connected devices, to see the scalability degree under
such figure of merit.

