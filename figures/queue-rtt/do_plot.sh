#!/bin/bash

# Specify where the data is.
DATA_DIR=/home/nat/Work/paper/work-bp-tcp/results
TCP=Cubic
CONN=terr # sat is the other accepted value
SLOW=ss   # ca is the other accepted value
## END OF CONFIGURATION

# Per-flow is -1-3-, while per-packet is -1-2-
flowfile20=${DATA_DIR}/20/out-Tcp${TCP}-1-3-${CONN}-${SLOW}/lte-multiradio-1-rtt.data
flowfile200=${DATA_DIR}/200/out-Tcp${TCP}-1-3-${CONN}-${SLOW}/lte-multiradio-1-rtt.data
pktfile20=${DATA_DIR}/20/out-Tcp${TCP}-1-2-${CONN}-${SLOW}/lte-multiradio-1-rtt.data
pktfile200=${DATA_DIR}/200/out-Tcp${TCP}-1-2-${CONN}-${SLOW}/lte-multiradio-1-rtt.data

echo -en "set output \"rtt-comparison-${TCP}-${CONN}-${SLOW}.eps\"\n" > ${1}_tmp.gnuplot
echo -en "set ylabel \"RTT (ms)\"\n" >> ${1}_tmp.gnuplot
echo -en "plot " >> ${1}_tmp.gnuplot
echo -en "\"${flowfile20}\" u 1:(\$2*1e3) title \"per-flow, 20 pkt\" w l ls 2," >> ${1}_tmp.gnuplot
echo -en "\"${pktfile20}\" u 1:(\$2*1e3) title \"per-packet, 20 pkt\" w l ls 4," >> ${1}_tmp.gnuplot
echo -en "\"${flowfile200}\" u 1:(\$2*1e3) title \"per-flow, 200 pkt\" w l ls 1," >> ${1}_tmp.gnuplot
echo -en "\"${pktfile200}\" u 1:(\$2*1e3) title \"per-packet, 200 pkt\" w l ls 3" >> ${1}_tmp.gnuplot
cat ${DATA_DIR}/../throughput.gnuplot ${1}_tmp.gnuplot | gnuplot
rm -f ${1}_tmp.gnuplot

