Bringing the network close to the users is the leading technique for improving
coverage and spectrum efficiency in modern 5G
deployments~\cite{webb2007wireless}. In fact, the rising demand of wireless data
services can be accommodated by dense, and portable, \gls{lte} deployments.
After a disaster, this is also one of the most effective strategy to build a
temporary network to support the first responder operators during the emergency.
One of the main
challenges in this scenario is the traffic backhauling, a problem that can be
divided in two design phases: (i) choose the transport technology and (ii)
choose a routing protocol/load balancing scheme that can exploit the strengths
of the transport technology.

The first phase, being unlikely to have fiber connections for every access node
(in \gls{lte} terminology, it is called eNodeB), especially after a disaster,
can be addressed with wireless transport nodes, a possibility already detailed in~\cite{6963798}.
Each eNodeB will feature one or multiple \gls{ptp} backhaul
interfaces, hence enabling the creation of a path redundant backhaul \gls{wmn}.
In this way, LTE user and control plane traffic will be appropriately carried
from/towards the \gls{epc}. Coupled with the possibility of employing portable
\gls{lte} infrastructures for establishing coverage and increasing capacity of
existing networks, it is possible even to deploy self-organizing networks: each
\gls{sc} (\gls{enodeb} + wireless transport interface) can be assigned to a position,
and then connected through near \gls{sc}s forming a temporary ad hoc network
topology. This allows people and devices to seamlessly communicate in areas with
no pre-existing infrastructures (e.g., disaster recovery, battlefield
environments and so on) and, coupled with a satellite
backhaul~\cite{casoni2015integration}, to receive data from/to an external
network, such as Internet.

The second phase is centered about the routing strategy. In the aforementioned
context, key features of a routing strategy are the load balancing capabilities
(to fully exploit all deployed resources) and the network performance achieved
when the traffic volume substantially increases. One of the most relevant
performance parameters is the \gls{tcp} behavior (e.g., \gls{tcp} throughput),
given its predominance in the Internet due also to the increasing popularity of
on-line streaming services (e.g., YouTube, Netflix, Spotify). However,
throughput is not the only parameter that affects the end users' experience:
latency is important as well, if not critical in upcoming deployments.

\subsection{Related Work}
% When there is a path diversity inside the network, such as in the case of
% backhaul \gls{wmn}, the users can not exploit any transport level technique
% like \gls{mptcp}, because they are end-to-end and they can exploit only
% different end-to-end path (e.g., a transmission using simultaneously WiFi and
% 4G).
Load balancing could be conducted at the \gls{ran} segment. Indeed,
transport-layer protocols such as \gls{mptcp} can help balance traffic to
\gls{ue}s in multi-homed environments (e.g., a \gls{ue} equipped with both \gls{lte}
and WiFi). However, we focus on the study of load balancing backhaul traffic in
a single-homed \gls{ran} environment.

At the mobile backhaul segment, multi-path routing protocols operating at the IP layer can offer better resource
utilization, and better reliability, thanks to the possibility to exploit the
path diversity. For interested readers, there is a
complete survey on multi-path routing and provisioning in~\cite{7165586}.
Nevertheless, in mobile backhaul networks state of the art routing,
traditionally adopted, uses one single path between endpoints in the absence of
failures (i.e. \gls{mpls} depicted in RFC 5921 or \gls{olsr} presented in RFC 3626).
Moreover, congestion unawareness and unpredictability of mobile traffic patterns
makes single-path routing not efficient for a resource-constrained \gls{wmn}, as
it does not exploit the multiple paths provided by the mesh. By looking to the
literature, theoretical backpressure~\cite{7422412} offers the possibility to
exploit all backhaul resources (given its throughput optimality), one of the
main operator wishes towards a cost-efficient deployment.

For readers interested in the history of backpressure-based routing protocol,
in~\cite{7422412} there is a comprehensive survey of backpressure
state-of-the-art. The root concept consists in a centralized policy which
routes traffic in a multi-hop network by minimizing the sum of the queue
\emph{backlog}s in the network among time slots. Basically, if we define as
\emph{backlog} the queue size at nodes, the main idea of backpressure is to
give priority to links and paths that have higher differential \emph{backlog}
between neighboring nodes; from a network perspective, these strategies
dynamically map the trajectory followed by each data packet to the more
underutilized paths, hence making congestion-aware decisions. However, these
decisions may potentially make the path followed by consecutive packets of the
same flow disjoint. Despite the throughput optimality promise, many
backpressure-based protocols (included the original one) present problems which
limit their applicability in the real world, such as centralized control mode,
high queuing complexity due to the maintenance of a per-flow queuing system,
and poor delay performance. Recently, many proposals have been presented to
alleviate the effect of these issues~\cite{7422412}.

For the purpose of this paper, we are interested in the TCP
performance, degraded by the reordering typical of backpressure algorithms.
In~\cite{RadunovicHorizon}, it is proposed a delayed reordering algorithm at the
destination that eliminates TCP timeouts while keeping packet reordering to a
minimum. Furthermore, in~\cite{seferoglu2014tcp} it is shown that TCP
experiences incompatibilities with backpressure strategies that maintain
per-flow queues, hence leading to unfairness between flows. In contrast, in this
work we keep the TCP layer unmodified and we employ \emph{BP-MR}~\cite{7396680},
a backpressure strategy which has a simpler and more scalable implementation,
since it maintains per-interface queues. The per-flow variant of \emph{BP-MR}
was proposed in~\cite{en4ppdrrouting} with the objective to improve TCP
performance in an emergency scenario with a satellite backhaul. In~\cite{PATRICIELLO201740},
the variants were compared over a regular mesh and an irregular ring-tree
topology, and using many different TCP protocols. The work in~\cite{jordi} shows that
this variant (per-flow) of BP-MR is also suitable for hybrid
satellite-terrestrial LTE backhaul networks when using both UDP and TCP
transmission protocols.  
The main difference between the variants is that
\emph{per-packet} computes the best possible next-hop for each packet forwarded,
while \emph{per-flow} calculates it just for the first packet of a flow, and
then saving the pair next hop/flow in a forwarding table.

\subsection{Our Contribution}

In this paper, assuming dense deployments formed by a \gls{lte} \gls{ran}
backhauled by a \gls{wmn}, we aim to investigate the advantages and limitations
of qualitatively different routing strategies. In this direction, we use two
variants of a backpressure-based routing protocol (BP-MR~\cite{7396680} per-flow and per-packet) and
\gls{olsr}, comparing \gls{udp} and \gls{tcp} performance on top of these routing
strategies, focusing on the scalability of the system (in the sense of
evaluating the performance when the users increase and when the traffic
increases).

The conducted experiments allow concluding that reducing the granularity from
per-packet to per-flow decisions offers the best trade-off between resource
utilization and \gls{tcp} performance of \gls{lte} mobile device. More specifically,
simulation results obtained with ns-3~\footnote{https://www.nsnam.org} reveal
that the \emph{per-flow} \emph{BP-MR} variant offers the best TCP performance
despite its higher \gls{rtt}, since it experiences fewer losses
and reordering events, showing its potential in being a good routing protocol
candidate in non-mobile (referred to access nodes, not users) dense
deployments, without necessarily resorting to an unnecessary over-provisioning
of backhaul resources.


The remainder of this paper is organized as follows.
%Section~\ref{sec:granularity} contains a detailed explanation of the
%\emph{BP-MR} protocol and introduces its \emph{per-flow} variant,
Section~\ref{sec:scenario} presents the reference scenario and methodology
before showing the evaluation of \emph{BP-MR per-packet, per-flow}, and
\emph{OLSR} in Section~\ref{sec:observations}. Finally,
Section~\ref{sec:conclusion} concludes the paper.
