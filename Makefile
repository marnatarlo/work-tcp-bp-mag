# Platform, to accomodate Win/CygWin/Darwin needs.
#
# Comment by nat
PLATFORM=$(shell uname)

# Main target.
#
TARGET=bp-tcp-wirelessmag
TITLE="Towards scalable and flexible backpressure routing for carrying TCP in wireless mesh backhauls for dense LTE networks"
AUTHOR="Natale Patriciello, Jose Nunez-Martinez, Jorge Baranda, Maurizio Casoni, Josep Mangues-Bafalluy"
KEYWORD=""
# Source files: Be lazy and consider each .tex file as a source, except
# the ones we generate for "internal" use.
#
SOURCES:=bp-tcp-wirelessmag.tex

# Now we use a sub-makefile
#IMAGES:=

# Inkscape seems not to be widely used on Windows (and there people say that
# 80 columns hard limits are not so hard...)
#
ifeq ($(findstring $(PLATFORM), Linux FreeBSD),)
ifeq ($(findstring $(PLATFORM), Darwin),)
CYG=1
INKSCAPE=/cygdrive/c/Programmi/Inkscape/inkscape.exe
INKPATH=$(shell cygpath -w /cygdrive/c/Programmi/cygwin/home/paolo/condivisa/Pisa/netdev/mac-sal-paper/)
MYSH=dash
else
DIA:=/Applications/Dia.app/Contents/Resources/bin/dia -n
INKSCAPE:=/Applications/Inkscape.app/Contents/Resources/bin/inkscape
INKPATH:=
MYSH=sh
endif
else
DIA:=dia
INKSCAPE:=inkscape
INKPATH:=
MYSH=sh
endif

# Generate the commands needed to produce $(TARGET).dvi from $(TARGET).tex,
# being sure to rerun latex as many times as it wants.  They say "better to
# be safe than sorry," so we end up calling it more times than needed...
#
define gen-latex-rules
	while (latex $(TARGET) ; grep -q "Rerun to get cross" $(TARGET).log) \
	do true ; done
endef

# Default targets: All the .ps outputs (temporary, it may take too much time).
#
all:
	$(MAKE) -C figures
	$(MAKE) $(TARGET).pdf

$(TARGET).dvi: $(TARGET).bbl $(SOURCES)
	$(call gen-latex-rules)

%.ps: %.dvi
	dvips -t a4 $< -o $@

%.pdf: %.ps
	ps2pdf -dPDFSETTINGS=/prepress -dSubsetFonts=true -dEmbedAllFonts=true -dMaxSubsetPct=100 -dCompatibilityLevel=1.4 $(TARGET).ps
	exiftool -Title=$(TITLE) -Author=$(AUTHOR) $(TARGET).pdf

$(TARGET).bbl: $(TARGET).aux $(TARGET).bib
	bibtex $(TARGET)

$(TARGET).aux: $(SOURCES)
	$(call gen-latex-rules)

clean:
	rm -rf *.aux *.dvi *.log *.blg *.bbl labels.tex target.tex

cleanedit:
	rm -rf *~ _region_*

cleanall: clean cleanedit
	rm -rf *.pdf rm -rf *.ps
